Tasklist::Application.routes.draw do
  resources :tasks do
  	"tasks#order_by_priority"
  end
  resources :categories
  resources :authors

  root to: "tasks#index"
end
